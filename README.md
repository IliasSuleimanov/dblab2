Ильяс Сулейманов
Лабораторная работа №2
Группа: KP-42
Предметная область: средства для управления данными альбомов, песен, исполнителей.

Завдання роботи полягає у наступному:
1. Розробити схему бази даних на основі предметної галузі з ЛР№2-Ч1 у
спосіб, що застосовується в СУБД MongoDB.
2. Розробити модуль роботи з базою даних на основі пакету PyMongo.
3. Реалізувати дві операції на вибір із використанням паралельної обробки
даних Map/Reduce.
4. Реалізувати обчислення та виведення результату складного агрегативного
запиту до бази даних з використанням функції aggregate() сервера
MongoDB.

Текст функций map-reduce та aggregate:

       
```
#!python

        map_function = Code('''function(){
                       for(var i=0 in this.songs){
                           if(this.songs[i].price''' + equation + str(price) + '''){
                               emit(this.songs[i].price, 1);
                               }
                           }
                       }''')

        reduce_function = Code('''function(key, values){
                          var sum = 0;
                          for( var i=0 in values){
                              sum+=values[i];
                              }
                          return sum;
                          }''')


        map_function = Code('''function(){
                            for(var i = 0 in this.songs){
                            duration = this.songs[i].duration.split(':');
                            equation =  '''+ duration + '''.split(':');
                               if (duration[0]==equation[0]){
                               if (duration[1] + equation + equation[1])
                               emit(this.songs[i].duration, 1);
                               }
                               else{
                               if (duration[0] + equation + equation[0])
                               emit(this.songs[i].duration, 1);
                               }
                               }
                               }''')

        reduce_function = Code('''function(key, values){
                               var sum = 0;
                               for(var i=0 in values){
                               sum+=values[i];
                               }
                               return sum;
                               }''')
        aggregate([
                     {"$match": {'name': album_name}},
                     {"$unwind": '$songs'},
                     {"$group": {'_id': '$songs._id', "price":{"$first":"$songs.price"}}},
                     {"$group": {'_id': "album_price", "total": {"$sum": "$price"}}}
                  ])
```


![Снимок экрана от 2016-12-19 03:53:06.png](https://bitbucket.org/repo/8EG9Mx/images/2291461156-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%20%D0%BE%D1%82%202016-12-19%2003:53:06.png)![Снимок экрана из 2016-12-19 03:54:29.png](https://bitbucket.org/repo/8EG9Mx/images/1882394419-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B8%D0%B7%202016-12-19%2003:54:29.png)![Снимок экрана из 2016-12-19 03:54:29.png](https://bitbucket.org/repo/8EG9Mx/images/3210077743-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B8%D0%B7%202016-12-19%2003:54:29.png)